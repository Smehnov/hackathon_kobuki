
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import matplotlib.pyplot as plt
import numpy as np


import rospy
from sensor_msgs.msg import PointCloud2
#from sensor_msgs.msg import convertPointCloud2ToPointCloud
import sensor_msgs.point_cloud2 as pc2

def callback(data):
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	points =[]
	p_x = []
	p_y = []
	p_z = []
	i=0
	k=0
	for p in pc2.read_points(data, field_names = ("x", "y", "z"), skip_nans=True):

		if (-p[2])<1:
			k+=1
			points.append([p[0], p[1], -p[2]])
			if k%30 == 0 and i<10000:
				i+=1
				p_x.append(p[0])
				p_y.append(p[1])
				p_z.append(p[2])
				print(p[0], p[1], p[2])
	ax.scatter(p_x, p_z, p_y, marker = 'o')
	print(len(points))
	plt.show()
	
def main():
	rospy.init_node('depth_camera_listener', anonymous=True)
	rospy.Subscriber('/camera/depth/points', PointCloud2, callback)
	rospy.spin()

if __name__ == '__main__':
    main()

