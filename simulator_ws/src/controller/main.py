import math
import rospy
import numpy as np
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import matplotlib.pyplot as plt
import cv2

from sensor_msgs.msg import Image
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2 as pc2
from cv_bridge import CvBridge, CvBridgeError
evade_dist = 0.4
sensor_h = 0.375
robot_x2world = 5.5
robot_y2world = -5.7
robot_x = 0
robot_y = 0
robot_theta = 0
robot_vel_x = 0
robot_vel_y = 0
robot_omega = 0

cur_aim_dot_num = 0#[[2.9, -6.1], [0,-6],[0,0], [0, 2.55], [-1.3, 2.6]]#[ [0.5,-5.5], [0, 2.55], [-1.3, 2.6]]
dots = [[0.5,-5.5], [-1.25, 2.6]]#
bridge = CvBridge()

robot_aim_x = 0

robot_aim_y = -5.7

k_dist = 1.5
k_angle = 1.8
epsilon_coords = 0.1

evade_start_theta = 0
evade_state =  0

vision_points = np.array([[0,0,0]])

depth_im = None

is_showing_points = False
def callback_vision(data):
	global vision_points
	sin_alpha = math.sin(robot_theta)
	cos_alpha = math.cos(robot_theta)
	
	points =[]
	p_x = []
	p_y = []
	p_z = []
	i=0
	k=0
	
def callback_depth(data):
	global depth_im
	depth_im = bridge.imgmsg_to_cv2(data, "32FC1")
	print("GOT DEPTH IMAGE")
	pass

def callback_odom(data):
	global robot_x, robot_y, robot_theta
	pose = data.pose.pose

	robot_x = pose.position.x + robot_x2world
	robot_y = pose.position.y + robot_y2world
	robot_theta = 2*math.atan2(pose.orientation.z, pose.orientation.w)

def main():
	global robot_x, robot_y, robot_theta, robot_vel_x, robot_vel_y, robot_omega, robot_aim_x, robot_aim_y, cur_aim_dot_num, dots, evade_state, evade_start_theta
	rospy.init_node('robot_controller', anonymous=True)
	pub = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=10)
	rate = rospy.Rate(5)

	rospy.Subscriber('/camera/depth/image_raw', Image, callback_depth)
	rospy.Subscriber('/odom', Odometry, callback_odom)
	rospy.Subscriber('/camera/depth/points', PointCloud2, callback_vision)

	k = 0

	while(True):
		robot_aim_x = dots[0][0]
		robot_aim_y = dots[0][1]


		e_dist = math.sqrt((robot_aim_x-robot_x)**2 + (robot_aim_y-robot_y)**2)
		#if not (((robot_aim_x-epsilon_coords)<robot_x<(robot_aim_x+epsilon_coords)) and ((robot_aim_y-epsilon_coords)<robot_y<(robot_aim_y+epsilon_coords))):
		if e_dist>epsilon_coords:	
			alpha = math.atan2((robot_aim_y-robot_y), (robot_aim_x-robot_x))

			e_angle = (alpha - robot_theta)
			if abs(e_angle)>math.pi:
				e_angle -= math.pi*2*math.copysign(1,e_angle)
			robot_vel_x = 0.65 * math.tanh(e_dist*k_dist)#min(e_dist*k_dist, 0.65)
			robot_omega = e_angle*k_angle
			print("x", robot_x, "y", robot_y, "aim_x", robot_aim_x, "aim_y", robot_aim_y, "alpha", alpha)
			print("e_dist", e_dist, "e_angle", e_angle, "vel_x", robot_vel_x, "omega1", robot_omega)
		else:
			print("x", robot_x, "y", robot_y, "aim_x", robot_aim_x, "aim_y", robot_aim_y,)
			# cur_aim_dot_num+=1
			if(len(dots)>1):
				dots.pop(0)
			else:
				print("FINISH!!!")
				break
			robot_vel_x = 0
			robot_omega = 0
			print("Robot is in aim")
		#0-black
		#0.632687 - value of gray

		if depth_im!=None:
			depth_img = depth_im[:-60,]
			depth_img = np.nan_to_num(depth_img)
			print(depth_img[300,400])
			rgb = cv2.cvtColor(depth_img,cv2.COLOR_GRAY2RGB)


			hsv = cv2.cvtColor(rgb, cv2.COLOR_RGB2HSV)
			print(hsv[50,50])
			lower_gray = np.array([0, 0, 0.3])
			upper_gray = np.array([0, 0, 0.75])
			mask_gray = cv2.inRange(hsv, lower_gray, upper_gray)

			#print(mask_im)
			#cv2.imshow("Image window", depth_img[:,:]*mask)
			cv2.imshow("Image window", mask_gray)
			
			#avg_color_per_row = np.average(depth_im, axis=0)
			#avg_color = np.average(avg_color_per_row, axis=0)
			print("EVADE_STATE", evade_state)
			if evade_state==0:
				if mask_gray[:,50:-50].mean()>90:
					robot_vel_x = 0
					evade_start_theta = robot_theta
					evade_state = 1

				else:
					if mask_gray[-20:,-50:].mean()>75:
						robot_omega = 2
						robot_vel_x+=0.05
						

					if mask_gray[-20:,:50].mean()>75:
						robot_omega = -2
						robot_vel_x+=0.05



			elif evade_state==1:
				robot_vel_x = 0
				if mask_gray.mean()>90:
					robot_omega = 1
					angle_change = (evade_start_theta-robot_theta)
					if abs(angle_change)>math.pi:
						angle_change -= math.pi*2*math.copysign(1,angle_change)
					if angle_change>math.pi/2:
						evade_state = 2

				else:

					dot_x = robot_x + math.cos(robot_theta+math.pi/4)*evade_dist
					dot_y = robot_y + math.sin(robot_theta+math.pi/4)*evade_dist
					dots.insert(0,[dot_x, dot_y])
					evade_state = 0
			elif evade_state==2:
				robot_vel_x = 0
				
				if mask_gray.mean()>590:
					robot_omega = -1
					angle_change = -(evade_start_theta-robot_theta)
					if abs(angle_change)>math.pi:
						angle_change -= math.pi*2*math.copysign(1,angle_change)
					if angle_change>math.pi/2:
						evade_state = 3
				else:
					dot_x = robot_x
					dot_y = robot_y
					dots.insert([dot_x, dot_y], 0)
					evade_state = 0
			elif evade_state == 3:
				print("!!!!!!!!!!!!!!!!!!!")
				robot_vel_x = 0
				robot_omega = 0


			#print(np.nan_to_num(depth_img[0:,250:450]).mean())
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
			#cv2.waitKey(0)
		vel = Twist()
		vel.linear.x = robot_vel_x
		vel.linear.y = robot_vel_y
		vel.angular.z = robot_omega
		pub.publish(vel)

		rate.sleep()

		



if __name__ == '__main__':
	main()
